package com.hfut.edu.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import bitoflife.chatterbean.AppException;



public class GossipLoad  {


	
	static DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
   
	public static void createXml(String fileName,String pattern,String template)
	{
		 try {
			    DocumentBuilder db = dbf.newDocumentBuilder();
	            Document document = db.parse(fileName);
	            Element root=(Element) document.getElementsByTagName("aiml").item(0);
	            
	            System.out.println("解析完毕");
	            /* 创建一个完成的节点，category */
		        Element category = document.createElement("category");
		        Element patt= document.createElement("pattern");
		        patt.setTextContent(pattern);  
		        Element tem= document.createElement("template");
		        tem.setTextContent(template);
		        category.appendChild(patt);
		        category.appendChild(tem);
		        root.appendChild(category);
		      //将DOM对象document写入到xml文件中
		        TransformerFactory tf = TransformerFactory.newInstance();
		        try {
		            Transformer transformer = tf.newTransformer();
		            DOMSource source = new DOMSource(document);
		            transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
		            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		            PrintWriter pw = new PrintWriter(new FileOutputStream(fileName));
		            StreamResult result = new StreamResult(pw);
		            transformer.transform(source, result);     //关键转换
		            System.out.println("生成XML文件成功!");
		        } catch (TransformerConfigurationException e) {
		            System.out.println(e.getMessage());
		        } catch (IllegalArgumentException e) {
		            System.out.println(e.getMessage());
		        } catch (FileNotFoundException e) {
		            System.out.println(e.getMessage());
		        } catch (TransformerException e) {
		            System.out.println(e.getMessage());
		        }
	        } catch (FileNotFoundException e) {
	            System.out.println(e.getMessage());
	        } catch (ParserConfigurationException e) {
	            System.out.println(e.getMessage());
	        } catch (SAXException e) {
	            System.out.println(e.getMessage());
	        } catch (IOException e) {
	            System.out.println(e.getMessage());
	        }
		
		
	}
	/**
	 * 构造一个xml
	 * 
	 * @throws IOException
	 */
	public static void load(String parentPath) throws IOException {
		String gossipPath=parentPath+"/gossip.txt";
		String destination=parentPath+"/TestChinese/gossip.xml";
		System.out.println("读取的地址是   "+gossipPath);
		System.out.println("写入的目的地址是   "+destination);
		InputStream inputStream =new FileInputStream(new File(gossipPath));
		BufferedReader bufReader =  new BufferedReader(new InputStreamReader(inputStream));;
		String line;
		String pattern="";
		String template="";
		
		StringBuffer stringBuf = new StringBuffer();

		while ((line = bufReader.readLine()) != null) {
			pattern=line.split(":")[0];
			template=line.split(":")[1];
		}
		inputStream.close();
		bufReader.close();
		createXml(destination,pattern,template);
	}

	/**
	 * 构造一个category
	 * 
	 * @param line
	 * @return
	 */
	private static String analyzing(String line) {
		String[] pattern = line.split(":");
		return "<category><pattern>" + pattern[0] + "</pattern><template>"
				+ pattern[1] + "</template></category>";
	}

	

	public static void main(String[] args) {
		for(int i=0;i<10;i++)
		{
			createXml("gossip.xml", "问题"+i, "回答"+i);
		}
	}
}
