package com.hfut.edu.util;



import net.sf.json.JSONObject;

import com.hfut.edu.bean.ResultBean;

public class CreateJson {
	
	public static String  create(ResultBean result)
	{
		JSONObject jsonObject = JSONObject.fromObject(result);
		return jsonObject.toString();
	}

}
