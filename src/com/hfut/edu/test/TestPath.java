package com.hfut.edu.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;

public class TestPath {
	
	public void OutsidePath() throws IOException
	{
		File file=new File("Corpus/readme.txt");
		System.out.println("OutsidePath是"+file.getAbsolutePath());
		FileReader fr=new FileReader(file);
		BufferedReader br=new BufferedReader(fr);
		String temp="";
		while((temp=br.readLine())!=null)
		{
			System.out.println("OutsidePath读取到的文件内容为"+ temp);
		}
		br.close();
		
		
	}
    public void InsidePath() throws IOException
    {
    	URL fileURL=this.getClass().getResource("/Corpus");    
        System.out.println(fileURL.getPath());  
    	InputStream is=this.getClass().getResourceAsStream("/Corpus/readme.txt");
    	InputStreamReader isr = new InputStreamReader(is);
    	BufferedReader br=new BufferedReader(isr);
    	String temp="";
		while((temp=br.readLine())!=null)
		{
			System.out.println("InsidePath读取到的文件内容为"+ temp);
		}
		br.close();
		

    }
	
	public static void main(String[] args) throws IOException {
		
		TestPath tp=new TestPath();
		//tp.OutsidePath();
		tp.InsidePath();
	}

}
