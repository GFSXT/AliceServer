package com.hfut.edu.test;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import javax.jws.WebService;
import com.hfut.edu.bean.ResultBean;
import com.hfut.edu.util.CreateJson;
import bitoflife.chatterbean.AliceBot;
import bitoflife.chatterbean.AliceBotMother;

@javax.jws.WebService(targetNamespace = "http://test.edu.hfut.com/", serviceName = "TestBotMotherService", portName = "TestBotMotherPort", wsdlLocation = "WEB-INF/wsdl/TestBotMotherService.wsdl")
public class TestBotMotherDelegate {

	com.hfut.edu.test.TestBotMother testBotMother = new com.hfut.edu.test.TestBotMother();

	public String responsd(String question) {
		return testBotMother.responsd(question);
	}

	public void main(String[] args) {
		testBotMother.main(args);
	}

}