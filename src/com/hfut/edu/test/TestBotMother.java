package com.hfut.edu.test;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hfut.edu.bean.ResultBean;
import com.hfut.edu.util.CreateJson;

import bitoflife.chatterbean.AliceBot;
import bitoflife.chatterbean.AliceBotMother;
@WebService
public class TestBotMother {
	
	private final static AliceBotMother mother = new AliceBotMother();	  
	private static AliceBot bot=null;
	private static Logger logger = LoggerFactory.getLogger(TestBotMother.class);
	static 
	  {	 
	    try {
	    	
			bot = mother.newInstance();
		} catch (UnsupportedEncodingException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
	  }

	
	  public static String responsd(String question)
	  {
		  String result="";
		  String[] temp=null;
		  if(bot!=null)
		  {
			  ResultBean rb=new ResultBean();			 
			  result=bot.respond(question);
			  logger.info("问题是"+question+"   "+"答案是"+result);
			  //如果是以@符号开头，说明匹配到了笑话与天气
			  if(result!=null && result.startsWith("@"))
			  {
				  temp=result.split(",");
				  //匹配到笑话，按照返回Json的标签说明txt构造Json数据
				  if(temp.length==2 && temp[1].equals("JOKE"))
				  {
					  rb.setFlag(2);
					  rb.setServiceCategory("Joke");
					  rb.setServiceContent("");
					  rb.setData("");
					  
				  }
				  //匹配到天气
				  if(temp.length==3 && temp[1].equals("Weather"))
				  {
					  rb.setFlag(3);
					  rb.setServiceCategory("Weather");
					  rb.setServiceContent(temp[2].replaceAll(" ",""));
					  rb.setData("");
				  }
			  }
			  //说明Alice模板没匹配到相关内容
			  if(result!=null && result.startsWith("#"))
			  {
				  rb.setFlag(0);
				  rb.setServiceCategory("QASystem");
				  rb.setServiceContent("");
				  rb.setData("");
			  }
			  //剩下一种情况只能匹配到闲聊了
			  if(result!=null && !result.startsWith("@") && !result.equals("#"))
			  {
				  rb.setFlag(1);
				  rb.setServiceCategory("Alice");
				  rb.setServiceContent("");
				  rb.setData(result.replaceAll(" ",""));
			  }
			  
			  return CreateJson.create(rb);
		  }
		  
		  //如果bot出现问题，直接返回""
		  return result;
		  
	  }
	  public static void main(String[] args) {
	        /**
	         * 参数1：服务的发布地址
	         * 参数2：服务的实现者
	         *  Endpoint  会重新启动一个线程
	         */
	        Endpoint.publish("http:/localhost/", new TestBotMother());
	        System.out.println("Server ready...");
	    }

	

}
