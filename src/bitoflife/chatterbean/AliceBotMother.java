/*
Copyleft (C) 2005 H�lio Perroni Filho
xperroni@yahoo.com
ICQ: 2490863

This file is part of ChatterBean.

ChatterBean is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

ChatterBean is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ChatterBean (look at the Documents/ directory); if not, either write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA, or visit (http://www.gnu.org/licenses/gpl.txt).
 */

package bitoflife.chatterbean;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import bitoflife.chatterbean.parser.AliceBotParser;
import bitoflife.chatterbean.parser.AliceBotParserConfigurationException;
import bitoflife.chatterbean.parser.AliceBotParserException;
import bitoflife.chatterbean.util.Searcher;

public class AliceBotMother{
	/*
	 * Attribute Section
	 */
	private String context;
	private String splitters;
	private String substitutions;
	private String chinese;
	private ByteArrayOutputStream gossip = null;
	private AliceBotParser parser=null;
	private AliceBot bot = null;
	private Searcher searcher = new Searcher();
	private static Logger logger = LoggerFactory.getLogger(AliceBotMother.class);

	public String gossip() {
		return gossip.toString();
	}

	public AliceBot newInstance() throws UnsupportedEncodingException {

		try {
			parser = new AliceBotParser();
			String parent=new File(this.getClass().getResource("/").getFile(),"../").getCanonicalPath();			
			context = URLDecoder.decode(parent+"/Corpus/context.xml", "utf-8");
			splitters = URLDecoder.decode(parent+"/Corpus/splitters.xml", "utf-8");
			substitutions = URLDecoder.decode(parent+"/Corpus/substitutions.xml", "utf-8");
			chinese = URLDecoder.decode(parent+"/Corpus/TestChinese", "utf-8");			
			bot = parser.parse(new FileInputStream(context),
					new FileInputStream(splitters),
					new FileInputStream(substitutions),
					searcher.search(chinese, ".*\\.xml"));
			
			
		} catch (AliceBotParserConfigurationException e) {
			logger.error("AliceBotParserConfigurationException  "+e.getMessage());
			throw new AppException(e);
		} catch (FileNotFoundException e) {
			logger.error("FileNotFoundException  "+e.getMessage());
			throw new AppException("[ExceptionInfo]相关文件没有找到。", e);
		} catch (AliceBotParserException e) {
			logger.error("AliceBotParserException  "+e.getMessage());
			throw new AppException(e);
		} catch (IOException e) {
			logger.error("IOException  "+e.getMessage());
			throw new AppException(e);
		}
		return bot;
	}
	
	
}
