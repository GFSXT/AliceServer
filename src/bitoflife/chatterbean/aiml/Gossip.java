/*
Copyleft (C) 2005 H�lio Perroni Filho
xperroni@yahoo.com
ICQ: 2490863

This file is part of ChatterBean.

ChatterBean is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

ChatterBean is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ChatterBean (look at the Documents/ directory); if not, either write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA, or visit (http://www.gnu.org/licenses/gpl.txt).
*/

package bitoflife.chatterbean.aiml;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;

import org.xml.sax.Attributes;

import com.hfut.edu.util.GossipLoad;

import bitoflife.chatterbean.AliceBot;
import bitoflife.chatterbean.Context;
import bitoflife.chatterbean.Match;

public class Gossip extends TemplateElement
{
  /*
  Constructors
  */

  public Gossip(Attributes attributes)
  {
  }

  public Gossip(Object... children)
  {
    super(children);
  }
  
  /*
  Methods
  */
  
  public String process(Match match)
  {
    AliceBot bot = null;
    Context context = null;
    if (match != null) try
    {
      bot = match.getCallback();
      context = bot.getContext();
      context.print(super.process(match));
//      URL fileURL=this.getClass().getResource("/Corpus");
//	  String path=fileURL.getPath();
	  GossipLoad.load("Corpus");
    }
    catch (Exception e)
    {
      e.printStackTrace();
      java.lang.System.out.println(e.getMessage());
      //throw new RuntimeException(e);
    }
    
    return "";
  }
}
