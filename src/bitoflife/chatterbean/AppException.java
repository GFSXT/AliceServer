package bitoflife.chatterbean;

import java.io.FileNotFoundException;
import java.io.IOException;

import bitoflife.chatterbean.parser.AliceBotParserConfigurationException;
import bitoflife.chatterbean.parser.AliceBotParserException;

public class AppException extends RuntimeException {
	
	public AppException()
	{
		
	}

	public AppException(AliceBotParserConfigurationException e) {
		// TODO 自动生成的构造函数存根
		super(e);
		System.out.println(e.getMessage());
	}

	public AppException(AliceBotParserException e) {
		// TODO 自动生成的构造函数存根
		super(e);
		System.out.println(e.getMessage());
	}

	public AppException(String string, FileNotFoundException e) {
		// TODO 自动生成的构造函数存根
		super(e);
		System.out.println(string);
	}

	public AppException(IOException e) {
		// TODO 自动生成的构造函数存根
		super(e);
		System.out.println(e.getMessage());
	}
	

}
